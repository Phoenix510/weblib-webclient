﻿using Shared;

var client = new ApiClient();

while (true)
{
    var fact = await client.GetRandomFact();
    Console.WriteLine("----------------------------------");
    Console.WriteLine("Random Fact:");
    Console.WriteLine(fact);

    Console.WriteLine("\nPress Enter to continue | Press any other key to exit");
    Console.WriteLine("----------------------------------\n");
    if (Console.ReadKey().Key != ConsoleKey.Enter)
        return 0;
}
