﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebLib;

namespace WebLibTests
{
    public class UrlBuilderTests
    {
        [Fact]
        public void URL_without_params_is_build_correctly()
        {
            var sut = new URLBuilder("http://basepath.com/", "test-end-point");

            var result = sut.Build();

            Assert.Equal("http://basepath.com/test-end-point", result.AbsoluteUri);

            sut = new URLBuilder("http://basepath.com/", "/test-end-point/");

            result = sut.Build();

            Assert.Equal("http://basepath.com/test-end-point/", result.AbsoluteUri);
        }

        [Fact]
        public void URL_query_is_build_correctly()
        {
            var sut = new URLBuilder("http://basepath.com/", "test-end-point", new() { { "ok", "test" }, { "test", "żą" } });

            var result = sut.Build();

            Assert.Equal("http://basepath.com/test-end-point?ok=test&test=%C5%BC%C4%85", result.AbsoluteUri);

            sut = new URLBuilder("http://basepath.com/", "/test-end-point/", new() { { "ok", "test" }, { "test", "żą" } });

            result = sut.Build();

            Assert.Equal("http://basepath.com/test-end-point/?ok=test&test=%C5%BC%C4%85", result.AbsoluteUri);
        }

        [Fact]
        public void URL_without_relative_part_is_build_correctly()
        {
            var sut = new URLBuilder("http://test.ok", null);

            var result = sut.Build();

            Assert.Equal("http://test.ok/", result.AbsoluteUri);

            sut = new URLBuilder("http://test.ok", null, new() { { "ok", "test" }, { "test", "żą" } });

            result = sut.Build();

            Assert.Equal("http://test.ok/?ok=test&test=%C5%BC%C4%85", result.AbsoluteUri);
        }
    }
}
