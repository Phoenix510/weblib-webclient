# WebLib - WebClient
## Simple library used to makes HTTP requests in .NET

WebClient uses HttpClient class and provides simple methods to call HTTP requests 
and receive the response in the specified format.

## Features
At this moment library supports following asynchronous requests:

- Get
- Post
- Put
- Delete

You can also add and remove headers from requests by using:
```sh
- AddHeader(string name, string value)
- RemoveHeader(string name)
- SetAuthorizationHeader(string value) // generates header with name "Authorization" and value: "Bearer [value]"
- GetAuthorizationHeader()
```

## Example usage

```sh
var client = new WebClient("https://catfact.ninja/");
var result = await client.GetAsync<CatFact>("/fact",
                new Dictionary<string, string>() { { "max_length", "120" } });
```

## License

MIT