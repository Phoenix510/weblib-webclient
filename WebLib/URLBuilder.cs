﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("WebLibTests")]
namespace WebLib
{
    internal class URLBuilder
    {
        private readonly string baseURI;
        private readonly string relativeURI;
        private readonly Dictionary<string, string> parameters;

        public URLBuilder(string baseURI, string relativeURI = null, Dictionary<string, string> parameters = null)
        {
            this.baseURI = baseURI;
            this.relativeURI = relativeURI ?? string.Empty;
            this.parameters = parameters ?? new Dictionary<string, string>();
        }

        public Uri Build()
        {
            var uriBuilder = new UriBuilder(baseURI);

            var query = HttpUtility.ParseQueryString(string.Empty);
            foreach (var parameter in parameters)
            {
                query.Add(parameter.Key, parameter.Value);
            }

            uriBuilder.Path = relativeURI;
            uriBuilder.Query = query.ToString();

            return uriBuilder.Uri;
        }
    }
}
