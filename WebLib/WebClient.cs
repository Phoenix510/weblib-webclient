using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebLib
{
    public class WebClient
    {
        public HttpClient client;
        public readonly string serverUri;

        public WebClient(string uri, HttpClient client = null)
        {
            this.client = client ?? new HttpClient();
            serverUri = uri;
            headersHelper = new HeadersHelper(this.client);
            if (serverUri is null)
                throw new ArgumentNullException("serverUri", "serverUri cannot be null");
        }

        #region HeadersOperations
        private readonly HeadersHelper headersHelper;

        public void AddHeader(string name, string value)
        {
            headersHelper.AddHeader(name, value);
        }

        public void RemoveHeader(string name)
        {
            headersHelper.RemoveHeader(name);
        }

        public bool SetAuthorizationHeader(string value)
        {
            return headersHelper.SetAuthorizationHeader(value);
        }

        public (string name, string value) GetAuthorizationHeader()
        {
            return headersHelper.GetAuthorizationHeader();
        }
        #endregion

        #region Post
        public async Task<T> PostAsync<T>(string uri, string toSend,
            Dictionary<string, string> paramerters = null, CancellationToken cancellationToken = default) where T : class
        {
            return await ErrorHandler<T>(() => postFunc(client, serverUri, paramerters, new object[] { uri, toSend }, cancellationToken));
        }

        public async Task<T> PostAsync<T>(string uri, HttpContent toSend, Dictionary<string, string> paramerters = null,
            CancellationToken cancellationToken = default) where T : class
        {
            return await ErrorHandler<T>(() =>
            postFunc(client, serverUri, paramerters, new object[] { uri, toSend }, cancellationToken));
        }

        Func<HttpClient, string, Dictionary<string, string>, object[], CancellationToken, Task<HttpResponseMessage>> postFunc = (HttpClient client,
            string serverUri, Dictionary<string, string> paramerters, object[] args, CancellationToken cancellationToken) =>
        {
            if (args[1] is HttpContent)
                return client.PostAsync(new URLBuilder(serverUri, args[0].ToString(), paramerters).Build(), args[1] as HttpContent);

            StringContent data = new StringContent($"{args[1]}", Encoding.UTF8, "application/json");
            return client.PostAsync(new URLBuilder(serverUri, args[0].ToString(), paramerters).Build(), data, cancellationToken);
        };
        #endregion

        #region Put
        public async Task<T> PutAsync<T>(string uri, string toSend, Dictionary<string, string> paramerters = null,
            CancellationToken cancellationToken = default) where T : class
        {
            return await ErrorHandler<T>(
                () =>
                {
                    StringContent data = new StringContent(toSend, Encoding.UTF8, "application/json");
                    return client.PutAsync(new URLBuilder(serverUri, uri, paramerters).Build(), data, cancellationToken);
                });
        }
        #endregion

        #region Get
        public async Task<T> GetAsync<T>(string uri, Dictionary<string, string> paramerters = null, 
            CancellationToken cancellationToken = default) where T : class
        {
            return await ErrorHandler<T>(() => client.GetAsync(new URLBuilder(serverUri, uri, paramerters).Build(), cancellationToken));
        }
        #endregion

        #region Delete
        public async Task<T> DeleteAsync<T>(string uri, Dictionary<string, string> paramerters = null,
            CancellationToken cancellationToken = default) where T : class
        {
            return await ErrorHandler<T>(() => client.DeleteAsync(new URLBuilder(serverUri, uri, paramerters).Build(), cancellationToken));
        }
        #endregion

        public static bool IsSuccessStatusCode(int code)
        {
            if (code > 199 && code < 300)
                return true;
            else
                return false;
        }

        private static async Task<T> ErrorHandler<T>(Func<Task<HttpResponseMessage>> function) where T : class
        {
            var result = await function();
            if (!result.IsSuccessStatusCode)
                throw new APIException(result.RequestMessage.RequestUri, result.ReasonPhrase, await result.Content.ReadAsStringAsync(), result.StatusCode);
            if (typeof(T) == typeof(string))
                return (await result.Content.ReadAsStringAsync()) as T;
            return await result.Content.ReadAsAsync<T>();
        }
    }
}
