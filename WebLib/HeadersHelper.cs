﻿using System.Linq;
using System.Net.Http;

namespace  WebLib
{
    internal class HeadersHelper
    {
        private readonly HttpClient client;
        private const string AuthorizationHeaderName = "Authorization";

        public HeadersHelper(HttpClient client)
        {
            this.client = client;
        }

        public void AddHeader(string name, string value)
        {
            var exists = client.DefaultRequestHeaders.Contains(name);
            if (!exists)
                client.DefaultRequestHeaders.Add(name, value);
        }

        public void RemoveHeader(string name)
        {
            var exists = client.DefaultRequestHeaders.Contains(name);
            if (exists)
                client.DefaultRequestHeaders.Remove(name);
        }

        public bool SetAuthorizationHeader(string value)
        {
            var exists = client.DefaultRequestHeaders.Contains(AuthorizationHeaderName);
            if (exists)
                RemoveHeader(AuthorizationHeaderName);
            AddHeader(AuthorizationHeaderName, $"Bearer {value}");
            return true;
        }

        public (string name, string value) GetAuthorizationHeader()
        {
            var exists = client.DefaultRequestHeaders.Where(h => h.Key == "Authorization")
                .FirstOrDefault();
            return (exists.Key, exists.Value.First());
        }
    }
}