﻿using System;
using System.Net;

namespace WebLib
{
    public class APIException : Exception
    {
        public APIException(Uri requestURL, string reasonPhrase, string responseContent,
                            HttpStatusCode statusCode) : base()
        {
            StatusCode = statusCode;
            RequestURL = requestURL;
            Message = $"{reasonPhrase}: {responseContent}";
        }

        public HttpStatusCode StatusCode { get; set; }
        public Uri RequestURL { get; set; }
        public override string Message { get; }
    }
}

