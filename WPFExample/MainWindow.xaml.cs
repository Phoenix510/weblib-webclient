﻿using Shared;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace TestWPFExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ApiClient apiClient;
        ObservableCollection<CatFact> catFacts;

        public MainWindow()
        {
            InitializeComponent();
            InitializeView();
            InitializeApiClient();       
        }

        void InitializeView()
        {
            catFacts = new();
            factsContainer.ItemsSource = catFacts;
        }

        void InitializeApiClient()
        {
            apiClient = new();
        }

        public async void GetRandomFact(object sender, EventArgs eventArgs)
        {
            catFacts.Add(await apiClient.GetRandomFact());
        }
    }
}
