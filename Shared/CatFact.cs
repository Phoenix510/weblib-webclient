﻿namespace Shared
{
    public class CatFact
    {
        public string Fact { get; set; }
        public int Length { get; set; }

        public override string ToString() => $"{Fact} | Length: {Length}";        
    }
}
