﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebLib;

namespace Shared 
{
    public class ApiClient
    {
        WebClient webClient;

        public ApiClient()
        {
            webClient = new WebClient("https://catfact.ninja/");
        }

        public async Task<CatFact> GetRandomFact()
        {
            var catFact =
                await webClient.GetAsync<CatFact>("/fact",
                new Dictionary<string, string>() { { "max_length", "120" } });

            return catFact;
        }
    }
}
